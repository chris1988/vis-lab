CREATE TABLE category
(
    category_id INT UNSIGNED PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL
);
CREATE TABLE customer
(
    customer_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    credential VARCHAR(255) NOT NULL,
    admin TINYINT DEFAULT 0 NOT NULL
);
CREATE TABLE product
(
    product_id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    category_id INT NOT NULL,
    price DECIMAL(11,2) NOT NULL,
    description VARCHAR(4095) NOT NULL
);
CREATE UNIQUE INDEX customer_id ON customer ( customer_id );
