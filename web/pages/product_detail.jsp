<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:body>
        <div class="row">
            <div class="medium-12 columns">
                <h2><s:property value="product.name"/> <small><s:property value="product.price"/>€</small></h2>
                <p><s:property value="product.description"/></p>
                <p><i class="fa fa-tag"></i> <s:property value="product.category.name"/></p>
                <s:if test="#session.admin eq true">
                <ul class="button-group">
                    <s:url var="editProduct" action="edit" namespace="/product">
                        <s:param name="product_id" value="product.productId" />
                    </s:url>
                    <s:a href="%{editProduct}" cssClass="button">Edit product</s:a>
                    <li>
                        <s:url var="deleteProduct" action="delete" namespace="/product">
                            <s:param name="product_id" value="product.productId" />
                        </s:url>
                        <s:a href="%{deleteProduct}" cssClass="button alert">Delete product</s:a>
                    </li>
                </ul>
                </s:if>
                <a href="<s:url action="all" namespace="/product" />" class="button success"><i class="fa fa-arrow-left"></i> Zurück zur Übersicht</a>
            </div>
        </div>
    </jsp:body>
</t:genericpage>

