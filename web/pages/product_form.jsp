<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:body>

        <div class="row">
            <div class="medium-12 columns">
                <s:if test="%{product!=null}">
                    <h2>Edit Product</h2>
                </s:if>
                <s:if test="%{product==null}">
                    <h2>Create new Product</h2>
                </s:if>
                <s:form action="edit" namespace="/product" method="post">
                    <label>Name
                        <s:textfield name="product.name" />
                    </label>
                    <label>Description
                        <s:textfield name="product.description"/>
                    </label>
                    <label>Price
                        <s:textfield name="product.price" placeholder="eg. 20.00"/>
                    </label>
                    <label>Category
                        <s:select name="product.category.categoryId" listKey="categoryId" listValue="name" list="categories"/>
                    </label>
                    <s:if test="%{product!=null}">
                        <s:hidden  name="product.productId" value="%{product.productId}"/>
                    </s:if>
                    <s:submit cssClass="button" value="Save"/>
                </s:form>
            </div>
        </div>
    </jsp:body>
</t:genericpage>

