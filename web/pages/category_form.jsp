<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:body>

        <div class="row">
            <div class="medium-12 columns">
                <s:if test="%{category!=null}">
                    <h2>Edit Category</h2>
                </s:if>
                <s:if test="%{category==null}">
                    <h2>Create new Category</h2>
                </s:if>
                <s:form action="edit" namespace="/category" method="post">
                    <label>Name
                        <s:textfield name="category.name" />
                    </label>
                    <s:if test="%{category!=null}">
                        <s:hidden  name="category.categoryId" value="%{category.categoryId}"/>
                    </s:if>
                    <s:submit cssClass="button" value="Save"/>
                </s:form>
            </div>
        </div>
    </jsp:body>
</t:genericpage>

