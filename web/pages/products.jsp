<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:body>

        <div class="row">
            <div class="medium-10 small-centered columns">
                <s:form namespace="/product" action="search" method="post">
                    <div class="row collapse">
                        <div class="small-4 columns">
                            <s:textfield key="query" placeholder="Search a product"></s:textfield>
                        </div>
                        <div class="small-2 columns">
                            <s:select headerKey="0" headerValue="All Categories" name="category_id" listKey="categoryId" listValue="name" list="categories" />
                        </div>
                        <div class="small-2 columns">
                            <s:textfield key="minPrice" placeholder="min price"></s:textfield>
                        </div>
                        <div class="small-2 columns">
                            <s:textfield key="maxPrice" placeholder="max price"></s:textfield>
                        </div>
                        <div class="small-2 columns">
                            <s:submit cssClass="button postfix" value="Search"/>
                        </div>
                    </div>
                </s:form>
            </div>
        </div>

        <div class="row">
            <div class="medium-9 columns">
                <s:iterator value="products">
                    <div class="row">
                        <div class="medium-12 columns">
                            <s:url var="showProduct" action="show" namespace="/product">
                                <s:param name="product_id" value="productId" />
                            </s:url>
                            <h2><s:a href="%{showProduct}"><s:property value="name"/></s:a> <small><s:property value="price"/>€</small></h2>
                            <p><s:property value="description"/></p>
                            <p><i class="fa fa-tag"></i> <s:property value="category.name"/></p>
                            <s:if test="#session.admin eq true">
                            <ul class="button-group">
                                <s:url var="editProduct" action="edit" namespace="/product">
                                    <s:param name="product_id" value="productId" />
                                </s:url>
                                <s:a href="%{editProduct}" cssClass="button small">Edit product</s:a>
                                <li>
                                    <s:url var="deleteProduct" action="delete" namespace="/product">
                                        <s:param name="product_id" value="productId" />
                                    </s:url>
                                    <s:a href="%{deleteProduct}" cssClass="button small alert">Delete product</s:a>
                                </li>
                            </ul>
                            </s:if>
                            <hr/>
                        </div>
                    </div>
                </s:iterator>
            </div>
            <div class="medium-3 columns">
                <s:if test="#session.admin eq true">
                    <div class="panel">
                        <h3><i class="fa fa-tags"></i> Categories</h3>
                        <hr/>
                        <s:iterator value="categories">
                            <div class="row">
                                <div class="medium-12 columns">
                                    <p><s:property value="name"/>
                                        <s:url var="editCategory" action="edit" namespace="/category">
                                            <s:param name="category_id" value="categoryId" />
                                        </s:url>
                                        <s:a href="%{editCategory}"><i class="fa fa-edit"></i></s:a>

                                        <s:url var="deleteCategory" action="delete" namespace="/category">
                                            <s:param name="category_id" value="categoryId" />
                                        </s:url>
                                        <s:a href="%{deleteCategory}"><i class="fa fa-times"></i></s:a>
                                    </p>
                                </div>
                            </div>
                        </s:iterator>
                    </div>
                </s:if>
            </div>

        </div>
    </jsp:body>
</t:genericpage>