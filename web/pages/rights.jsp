<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

</body>
</html>

<t:genericpage>
    <jsp:body>
        <h2>You have not the Rights to Edit or Update Data</h2>
    </jsp:body>
</t:genericpage>