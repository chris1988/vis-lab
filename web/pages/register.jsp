<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<s:set name="theme" value="'simple'" scope="page"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

</body>
</html>

<t:genericpage>
    <jsp:body>
        <div class="row">
            <div class="medium-6 large-centered columns">
                <h2>Registration</h2>

                <s:form action="register" method="post">
                    <label>Username
                        <s:textfield name="username" label="Enter User Name"/>
                    </label>
                    <label>Firstname
                        <s:textfield name="firstname" label="Enter Firstname"/>
                    </label>
                    <label>Lastname
                        <s:textfield name="lastname" label="Enter Lastname"/>
                    </label>
                    <label>Email
                        <s:textfield name="email" label="Enter Your Email"/>
                    </label>
                    <label>Password
                        <s:password name="credential" label="Enter Password"/>
                    </label>
                    <s:submit value="Register" cssClass="button"/>
                </s:form>
            </div>
        </div>

    </jsp:body>
</t:genericpage>