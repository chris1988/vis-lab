<%@tag description="Overall Page template" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s" %>

<s:set name="theme" value="'simple'" scope="page" />

<html>
<head>
    <link href="${pageContext.request.contextPath}/css/foundation.min.css" rel="stylesheet" type="text/css"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>
<body>



<nav class="top-bar" data-topbar>
    <ul class="title-area">
        <li class="name">
            <h1><a href="/">Vislab</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
    </ul>

    <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
            <s:if test="#session.userName != null">
                <!-- user logged in -->
                <li class="has-dropdown">
                    <a href="#"><i class="fa fa-user"></i> <s:property value="#session.userName" /></a>
                    <ul class="dropdown">
                        <li>
                            <s:url var="logout" namespace="/" action="logout" />
                            <s:a href="%{logout}"><i class="fa fa-sign-out"></i> Logout</s:a>
                        </li>
                    </ul>
                </li>
            </s:if>
            <s:else>
                <!-- user not logged in -->
                <li>
                    <s:url var="login" namespace="/" action="login" />
                    <s:a href="%{login}"><i class="fa fa-sign-in"></i> Login</s:a>
                </li>
                <li>
                    <s:url var="register" namespace="/" action="register" />
                    <s:a href="%{register}"><i class="fa fa-caret-square-o-up"></i> Register</s:a>
                </li>
            </s:else>
        </ul>

        <!-- Left Nav Section -->
        <ul class="left">
            <li>
                <a href="<s:url action="all" namespace="/product" />"><i class="fa fa-search"></i> Find Product</a>
            </li>
            <s:if test="#session.admin eq true">
            <li>
                <s:url var="create" namespace="/product" action="edit" />
                <s:a href="%{create}"><i class="fa fa-plus-square"></i> New Product</s:a>
            </li>
            <li>
                <s:url var="create" namespace="/category" action="edit" />
                <s:a href="%{create}"><i class="fa fa-plus-square"></i> New Category</s:a>
            </li>
            </s:if>
        </ul>
    </section>
</nav>

<!-- space -->
<div class="row"><div class="medium-12 columns"><br></div> </div>

<div class="row">
    <div class="medium-12 columns">
        <s:if test="hasActionErrors()">
            <div data-alert class="alert-box alert">
                <s:actionerror/>
                <a href="#" class="close">&times;</a>
            </div>
        </s:if>

        <s:if test="hasActionMessages()">
            <div data-alert class="alert-box success">
                <s:actionmessage/>
                <a href="#" class="close">&times;</a>
            </div>
        </s:if>
    </div>
</div>

<jsp:doBody/>




<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/vendor/jquery.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/foundation.min.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/foundation/foundation.alert.js" ></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/javascript/foundation/foundation.topbar.js" ></script>

<script>
    $(document).foundation();
</script>

</body>
</html>