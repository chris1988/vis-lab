package vislab.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.math.BigDecimal;


@FilterDefs({
        @FilterDef(name = "NAME_STRING_CONTAINS", parameters = {@ParamDef(name = "NAMES", type = "string")}),
        @FilterDef(name = "DESC_STRING_CONTAINS", parameters = {@ParamDef(name = "NAMES", type = "string")}),
        @FilterDef(name = "MIN_PRICE", parameters = {@ParamDef(name = "MIN_VALUE", type = "big_decimal")}),
        @FilterDef(name = "MAX_PRICE", parameters = {@ParamDef(name = "MAX_VALUE", type = "big_decimal")}),
        @FilterDef(name = "CATEGORY_ID_EQ", parameters = {@ParamDef(name = "CATEGORY_ID", type = "int")})
})

/**
 * Created by Chris on 02.04.14.
 */
@Entity

@Filters({
        @Filter(name = "NAME_STRING_CONTAINS", condition = "name like :NAMES"),
        @Filter(name = "DESC_STRING_CONTAINS", condition = "description like :NAMES"),
        @Filter(name = "MIN_PRICE", condition = ":MIN_VALUE <= price"),
        @Filter(name = "MAX_PRICE", condition = ":MAX_VALUE >= price"),
        @Filter(name = "CATEGORY_ID_EQ", condition = "category_id = :CATEGORY_ID")
})
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private int productId;

    private String name;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    private Category category;

    private BigDecimal price;
    private String description;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (category != product.category) return false;
        if (productId != product.productId) return false;
        if (description != null ? !description.equals(product.description) : product.description != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (price != null ? !price.equals(product.price) : product.price != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
