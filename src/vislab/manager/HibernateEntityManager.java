package vislab.manager;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import vislab.model.HibernateListener;
import vislab.model.Category;
import vislab.model.Customer;
import vislab.model.Product;

import javax.servlet.ServletContext;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Chris on 02.04.14.
 */
public class HibernateEntityManager implements EntityManager {


    private SessionFactory sessionFactory;
    private Session session;

    public HibernateEntityManager() {
        ServletContext context = ServletActionContext.getServletContext();
        sessionFactory =
                (SessionFactory) context.getAttribute(HibernateListener.KEY_NAME);
        session = sessionFactory.openSession();


    }

    @Override
    public List<Product> getAllProducts() {
        return session.createCriteria(Product.class).list();
    }

    @Override
    public List<Category> getAllCategories() {
        return session.createCriteria(Category.class).list();
    }

    @Override
    public List<Product> getProductsByDescription(String containment) {
        containment = "%" + containment + "%";
        session.enableFilter("DESC_STRING_CONTAINS").setParameter("NAMES", containment);
        List<Product> list = session.createCriteria(Product.class).list();
        session.disableFilter("DESC_STRING_CONTAINS");
        return list;
    }

    @Override
    public List<Product> getProductsByPriceInterval(BigDecimal minValue, BigDecimal maxValue) {
        session.enableFilter("MIN_PRICE").setParameter("MIN_VALUE", minValue);
        session.enableFilter("MAX_PRICE").setParameter("MAX_VALUE", maxValue);
        List<Product> list = session.createCriteria(Product.class).list();
        session.disableFilter("MIN_PRICE");
        session.disableFilter("MAX_PRICE");
        return list;
    }

    @Override
    public List<Product> findProducts(BigDecimal minValue, BigDecimal maxValue, String containment, int category_id) {
        if(minValue != null)
            session.enableFilter("MIN_PRICE").setParameter("MIN_VALUE", minValue);

        if(maxValue != null)
            session.enableFilter("MAX_PRICE").setParameter("MAX_VALUE", maxValue);

        if(category_id > 0)
            session.enableFilter("CATEGORY_ID_EQ").setParameter("CATEGORY_ID", category_id);

        containment = "%" + containment + "%";
        session.enableFilter("NAME_STRING_CONTAINS").setParameter("NAMES", containment);

        List<Product> list = session.createCriteria(Product.class).list();

        if(minValue != null)
            session.disableFilter("MIN_PRICE");

        if(maxValue != null)
            session.disableFilter("MAX_PRICE");

        session.disableFilter("NAME_STRING_CONTAINS");

        if(category_id > 0)
            session.disableFilter("CATEGORY_ID_EQ");

        return list;
    }

    @Override
    public List<Product> getProductsByCategory(int categoryId) {
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq("category.categoryId", categoryId));
        return criteria.list();

    }

    @Override
    public List<Product> getProductsByName(String containment) {
        containment = "%" + containment + "%";
        session.enableFilter("NAME_STRING_CONTAINS").setParameter("NAMES", containment);
        List<Product> list = session.createCriteria(Product.class).list();
        session.disableFilter("NAME_STRING_CONTAINS");
        return list;
    }

    @Override
    public Customer getCustomerByUsername(String username) {

        Criteria criteria = session.createCriteria(Customer.class);
        criteria.add(Restrictions.eq("username", username));
        List<Customer> list = criteria.list();
        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return session.createCriteria(Customer.class).list();
    }

    @Override
    public Customer getCustomer(int customerID) {
        return (Customer) session.get(Customer.class, customerID);
    }

    @Override
    public boolean addCustomer(Customer customer) {
        try {
            Transaction t = session.beginTransaction();
            session.save(customer);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getCause());
            return false;
        }

        return true;

    }

    @Override
    public Product getProduct(int productID) {
        return (Product) session.get(Product.class, productID);
    }

    @Override
    public boolean deleteProduct(int productID) {
        try {
            Transaction t = session.beginTransaction();
            session.delete(getProduct(productID));
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println(e.getCause());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteCategory(int categoryID) {
        try {
            Transaction t = session.beginTransaction();
            session.delete(getCategory(categoryID));
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            session.getTransaction().rollback();
            System.out.println(e.getCause());
            return false;
        }
    }

    @Override
    public boolean saveOrUpdateProduct(Product product) {
        try {
            Transaction t = session.beginTransaction();
            session.saveOrUpdate(product);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println(e.getCause());
            return false;
        }
    }

    @Override
    public boolean saveOrUpdateCategory(Category category) {
        try {
            Transaction t = session.beginTransaction();
            session.saveOrUpdate(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getCause());
            return false;
        }
        return true;
    }

    @Override
    public Category getCategory(int categoryID) {
        return (Category) session.get(Category.class, categoryID);
    }

    @Override
    public boolean addCategory(Category category) {
        try {
            Transaction t = session.beginTransaction();
            session.save(category);
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getCause());
            return false;
        }

        return true;
    }
}
