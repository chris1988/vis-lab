package vislab.manager;

import org.hibernate.annotations.ParamDef;
import vislab.model.Category;
import vislab.model.Customer;
import vislab.model.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Chris on 02.04.14.
 */
public interface EntityManager {
    /**
     *
     * @param customerID PK
     * @return the Customer
     */
    public Customer getCustomer(int customerID);

    /**
     *
     * @param username
     * @return the Customer
     */
    public Customer getCustomerByUsername(String username);

    /**
     *
     * @return allCustomers
     */
    public List<Customer> getAllCustomers();

    /**
     *
     * @param customer Id is auto generated
     * @return true if success
     */
    public boolean addCustomer(Customer customer);

    /**
     *
     * @param productID
     * @return the Product
     */
    public Product getProduct(int productID);

    /**
     * delete the product by id
     * @param productID
     * @return false if delete did not work, else true
     */
    public boolean deleteProduct(int productID);

    /**
     * delete the category by id
     * @param categoryID
     * @return false if delete did not work, else true
     * TODO what happens with products that have this category as id?
     */
    public boolean deleteCategory(int categoryID);

    /**
     *
     * @return all Products
     */
    public List<Product> getAllProducts();

    /**
     *
     * @param product Id is auto generated
     * @return true if success
     */
    public boolean saveOrUpdateProduct(Product product);

    /**
     *
     * @param categoryID
     * @return the category
     */
    public Category getCategory(int categoryID);

    /**
     *
     * @param category Id is auto generated
     * @return true if success
     */
    public boolean addCategory(Category category);

    /**
     *
     * @return all categories
     */
    public List<Category> getAllCategories();

    /**
     *
     * @param containment
     * @return List of all Products containing given String in name
     */
    public List<Product> getProductsByName(String containment);

    /**
     *
     * @param containment
     * @return List of all Products containing given String in description
     */

    public List<Product> getProductsByDescription(String containment);

    public List<Product> findProducts(BigDecimal minValue, BigDecimal maxValue, String containment, int category_id);

    /**
     *
     * @param minValue
     * @param maxValue
     * @return List of all Products with Price between min and Max
     */

    public List<Product> getProductsByPriceInterval(BigDecimal minValue, BigDecimal maxValue);

    /**
     *
     * @param categoryId
     * @return List of all Products in given Category
     */
    public List<Product> getProductsByCategory(int categoryId);


    boolean saveOrUpdateCategory(Category category);
}

