package vislab.security;

import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class LoginInterceptor implements Interceptor {
    private static final long serialVersionUID = 1L;
    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void init() {

        // TODO Auto-generated method stub

    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        Map<String, Object> sessionAttributes = invocation
                .getInvocationContext().getSession();




        //check User is logged in
        if (sessionAttributes == null
                || sessionAttributes.get("userName") == null) {
            return "login";
        } else {
            // check Admin Right if edit or delete
            if (!((String) sessionAttributes.get("userName")).equals(null)) {
                if (invocation.getProxy().getActionName().equals("edit") || invocation.getProxy().getActionName().equals("delete") ){
                    if ((sessionAttributes.get("admin") == null)){
                        return "insufficient_rights";

                    }
                }
                return invocation.invoke();
            } else {
                return "login";
            }
        }

    }

}
