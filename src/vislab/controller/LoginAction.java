package vislab.controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;
import vislab.manager.EntityManager;
import vislab.manager.HibernateEntityManager;
import vislab.model.Customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Chris on 05.04.14.
 */


public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<Customer> {
    private Map<String, Object> sessionMap;
    private Customer user = new Customer();


    @Override
    public void setSession(Map<String, Object> stringObjectMap) {
        this.sessionMap = stringObjectMap;
    }

    @Override
    @SkipValidation
    public String execute() throws Exception {
        return INPUT;
    }

    public String login() {
        String loggedUserName = null;

        // check if the userName is already stored in the session
        if (sessionMap.containsKey("userName")) {
            loggedUserName = (String) sessionMap.get("userName");
        }
        EntityManager em = new HibernateEntityManager();
        Customer existingUser = em.getCustomerByUsername(user.getUsername());


        if (loggedUserName != null && existingUser != null) {
            return SUCCESS;    // return welcome page
        }

        // if no userName stored in the session,
        // check the entered userName and password
        if (existingUser != null && user.getUsername() != null && user.getUsername().equals(existingUser.getUsername())
                && user.getCredential() != null && user.getCredential().equals(existingUser.getCredential())) {

            if (existingUser.isAdmin()) {
                sessionMap.put("admin", true);
            }
            // add userName to the session
            sessionMap.put("userName", user.getUsername());
            return SUCCESS;    // return welcome page
        }

        // in other cases, return login page
        if (!(user.getUsername() == null || user.getUsername().isEmpty())) {
            addActionError("Invalid User or Password!");
        }
        return INPUT;
    }

    public String logout() {
        // remove userName from the session
        if (sessionMap.containsKey("userName")) {
            sessionMap.remove("userName");
        }
        if (sessionMap.containsKey("admin")) {
            sessionMap.remove("admin");
        }
        addActionMessage("You have been signed out.");
        return SUCCESS;
    }


    public String register() {
        if (sessionMap.containsKey("userName")) {
            logout(); // Need to do that, if redirecting to login.jsp another Way would be to redirect to logoutAction after register
        }
        if (user.getUsername() == null) {
            return INPUT;
        }
        //TODO validate with xml
        EntityManager em = new HibernateEntityManager();
        if (em.addCustomer(user)) {
            addActionMessage("You have been successfully registered. Please login now.");
            return SUCCESS;
        }
        addActionError("Could not create your user account.");
        return ERROR;
    }

    @Override
    public Customer getModel() {
        return user;
    }

    @Override
    public void validate() {
        if (ActionContext.getContext().getName().equals("register")) {
            if (user.getUsername() == null || user.getUsername().isEmpty()) {
                addActionError("Please enter an username");
            } else {
                EntityManager em = new HibernateEntityManager();
                Customer existingUser = em.getCustomerByUsername(user.getUsername());
                if (existingUser != null) {
                    addActionError("Username already exists");
                }
            }
            if (user.getCredential() == null || user.getCredential().isEmpty()) {
                addActionError("Please enter a password");
            } else {
                if (user.getCredential().length() < 6) {
                    addActionError("Your password is weak please use at least 6 characters");
                }
            }
            if (user.getFirstname() == null || user.getFirstname().isEmpty()) {
                addActionError("Please enter your firstname");
            }
            if (user.getLastname() == null || user.getLastname().isEmpty()) {
                addActionError("Please enter your lastname");
            }
            if (user.getEmail() == null || user.getEmail().isEmpty()) {
                addActionError("Please enter your email");
            } else {
                if (new EmailValidator().validate(user.getEmail()) == false) {
                    addActionError("Your Email is not correct");
                }
            }


        }
        else if (ActionContext.getContext().getName().equals("login")){
            if (user.getUsername() == null || user.getUsername().isEmpty()) {
                addActionError("Please enter an username");
            }
            if (user.getCredential() == null || user.getCredential().isEmpty()) {
                addActionError("Please enter a password");
            }
        }
    }


}
