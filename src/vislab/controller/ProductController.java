package vislab.controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import vislab.manager.EntityManager;
import vislab.manager.HibernateEntityManager;
import vislab.model.Category;
import vislab.model.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by stefan on 02.04.14.
 */
public class ProductController extends ActionSupport {

    private int product_id; // for show action
    private List<Product> products; // for all products or search
    private Product product; // for new or edit product
    private String query; // search query string
    private List<Category> categories; // for list of all categories

    private int category_id; // for search form
    private String minPrice;
    private String maxPrice;

    private EntityManager manager;

    public ProductController() {
        super();
        manager = new HibernateEntityManager();
    }


    @Override
    public void validate() {

        if (ActionContext.getContext().getName().equals("edit")) {
            if (product != null) {
                if (product.getPrice() == null || product.getPrice().compareTo(BigDecimal.ZERO) < 0) {
                    addActionError("Price has to be a number and >= 0.");
                }
                if (product.getDescription() == null || product.getDescription().isEmpty()) {
                    addActionError("Please enter a description.");
                }
                if (product.getName() == null || product.getName().isEmpty()) {
                    addActionError("Please enter a name.");
                }
            }
        }

        if (ActionContext.getContext().getName().equals("search")) {
            if (minPrice != null && !minPrice.isEmpty()) {
                try {
                    BigDecimal min = new BigDecimal(minPrice);
                    if(min.compareTo(BigDecimal.ZERO) < 0)
                        addActionError("Price must be > 0");
                } catch (NumberFormatException exp) {
                    addActionError("Price must be a number.");
                }
            }
            if (minPrice != null && !maxPrice.isEmpty()) {
                try {
                    BigDecimal max = new BigDecimal(minPrice);
                    if(max.compareTo(BigDecimal.ZERO) < 0)
                        addActionError("Price must be > 0");
                } catch (NumberFormatException exp) {
                    addActionError("Price must be a number.");
                }
            }
        }

        //Fetch cagegories for jsp list
        categories = manager.getAllCategories();
    }

    /**
     * sets product to the product by id from the databse, returns SUCCESS
     * if not product with the given id could be found, it returns ERROR
     *
     * @return
     * @throws Exception
     */
    public String show() throws Exception {
        product = manager.getProduct(product_id);

        // check if a product with the given id could be found
        if (product != null) {
            return SUCCESS;
        } else {
            addActionError("Could not find a Product with the given ID.");
            // prepare vars for show all products page jsp
            fetchProductsAndCategories();
            return ERROR;
        }
    }

    /**
     * sets all products and categories from DB to display them
     *
     * @return
     * @throws Exception
     */
    public String all() throws Exception {
        // prepare all products
        fetchProductsAndCategories();

        return SUCCESS;
    }

    /**
     * puts search parameters to entity manager and sets all matching products
     *
     * @return
     */
    public String search() {
        BigDecimal min = null;
        BigDecimal max = null;
        if (!minPrice.isEmpty()) {
            min = new BigDecimal(minPrice);
        }
        if (!maxPrice.isEmpty()) {
            max = new BigDecimal((maxPrice));
        }
        products = manager.findProducts(min, max, query, category_id);
        categories = manager.getAllCategories();
        return SUCCESS;
    }

    /**
     * if product is set, this creates a new product in DB
     * if product is not set (user reached new product form), available categories are set
     * for the dropdown selection of category
     *
     * @return
     */
    public String saveOrUpdate() {
        if (product != null) {
            // product form is filled, try to save
            if (manager.saveOrUpdateProduct(product)) {
                addActionMessage("Product saved");
                // prepare vars for show all products page jsp
                fetchProductsAndCategories();
                return SUCCESS;
            } else {
                addActionError("Could not save changes");
                // prepare vars for show all products page jsp
                categories = manager.getAllCategories();
                return ERROR; // could not save
            }
        } else {
            // product form not filled
            // check if product_id is set, so the user wants to edit
            product = manager.getProduct(product_id);
            // user came to the product create form via link
            // prepare available categories to display dropdown in form
            categories = manager.getAllCategories();
            return INPUT;
        }
    }

    /**
     * delete product which matches the product id parameter
     *
     * @return
     */
    public String delete() {
        if (manager.deleteProduct(product_id)) {
            addActionMessage("Product deleted.");
            // prepare vars for show all products page jsp
            fetchProductsAndCategories();
            return SUCCESS;
        } else {
            addActionError("Could not delete the product.");
            // prepare vars for show all products page jsp
            fetchProductsAndCategories();
            return ERROR;
        }
    }

    private void fetchProductsAndCategories() {
        // prepare vars for show all products page jsp
        products = manager.getAllProducts();
        categories = manager.getAllCategories();
    }

    /*
        GETTERS AND SETTERS
     */

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }


    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}
