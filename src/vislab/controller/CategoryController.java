package vislab.controller;

import com.opensymphony.xwork2.ActionSupport;
import vislab.manager.EntityManager;
import vislab.manager.HibernateEntityManager;
import vislab.model.Category;
import vislab.model.Product;

import java.util.List;

/**
 * Created by stefan on 02.04.14.
 */
public class CategoryController  extends ActionSupport {

    private int category_id;

    private List<Product> products;
    private List<Category> categories;

    private Category category;

    private EntityManager manager;

    public CategoryController() {
        super();
        manager = new HibernateEntityManager();
    }

    public String edit() {
        if (category != null) {
            // product form is filled, try to save
            if (manager.saveOrUpdateCategory(category)) {
                addActionMessage("Category saved");
                // prepare vars for show all products page jsp
                fetchProductsAndCategories();
                return SUCCESS;
            } else {
                addActionError("Could not save changes");
                return ERROR; // could not save
            }
        } else {
            // product form not filled
            // check if product_id is set, so the user wants to edit
            category = manager.getCategory(category_id);
            return INPUT;
        }
    }

    public String delete() {
        if(manager.deleteCategory(category_id)) {
            fetchProductsAndCategories();
            addActionMessage("Category deleted.");
            return SUCCESS;
        } else {
            fetchProductsAndCategories();
            addActionError("Could not delete the category. Categories can not be deleted while they are used by any product.");
            return ERROR;
        }
    }

    private void fetchProductsAndCategories() {
        // prepare vars for show all products page jsp
        products = manager.getAllProducts();
        categories = manager.getAllCategories();
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
